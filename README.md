# README #

Matlab code for the simulation of crypto assets dynamics.
Details of the model implemented in the dynamics can be found here:
Bartolucci, Silvia, and Andrei Kirilenko. 
"A Model of the Optimal Selection of Crypto Assets." arXiv preprint arXiv:1906.09632 (2019).
https://arxiv.org/pdf/1906.09632.pdf 

Author: Silvia Bartolucci, s.bartolucci@imperial.ac.uk